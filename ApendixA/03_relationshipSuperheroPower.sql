CREATE TABLE superhero_power (
        super_id int,
        power_id int,
        PRIMARY KEY (super_id, power_id),
        CONSTRAINT fk_superhero FOREIGN KEY(super_id) REFERENCES superhero(super_id),
        CONSTRAINT fk_power FOREIGN KEY(power_id) REFERENCES power(power_id)
);
