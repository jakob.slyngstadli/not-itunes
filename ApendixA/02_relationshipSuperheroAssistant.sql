ALTER TABLE assistant
    ADD COLUMN fk_superhero INTEGER;

ALTER TABLE assistant
    ADD FOREIGN KEY(fk_superhero) REFERENCES Superhero(super_id);