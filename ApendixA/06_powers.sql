INSERT INTO Power (Power_Name, Description)
VALUES ('FireBall', 'Throw a ball of flame'),
       ('IceBall', 'Throw a ball of ice'),
       ('Covid', 'Curse that shuts down the world'),
       ('Java', 'Make the enemy confused and suicidal');


INSERT INTO superhero_power (super_id, power_id )
VALUES (1,1),
       (1,2),
       (1,3),
       (2,2),
       (3,2),
       (3,1);