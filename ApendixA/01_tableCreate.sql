DROP TABLE IF EXISTS Superhero;

CREATE TABLE Superhero (
                           super_id SERIAL PRIMARY KEY,
                           Superhero_Name varchar(50) NOT NULL,
                           Alias varchar(50) NOT NULL,
                           Origin varchar(50) NOT NULL
);

DROP TABLE IF EXISTS Assistant;

CREATE TABLE Assistant (
                           assist_id SERIAL PRIMARY KEY,
                           Assistant_Name varchar(50) NOT NULL
);

DROP TABLE IF EXISTS Power;

CREATE TABLE Power (
                       power_id SERIAL PRIMARY KEY,
                       Power_Name varchar(50) NOT NULL,
                       Description varchar(250) NOT NULL
);