package no.accelerate.notitunes.repository;

import no.accelerate.notitunes.models.Customer;
import no.accelerate.notitunes.models.CustomerCountry;
import no.accelerate.notitunes.models.CustomerGenre;
import no.accelerate.notitunes.models.CustomerSpender;

import java.util.List;

public interface CustomerRepository extends CRUDRepository<Customer, Integer> {
    /**
     * Takes in a customers First and Last name.
     * Returns customer object.
     * @param firstName
     * @param lastName
     * @return
     */
    Customer findByName(String firstName, String lastName);

    /**
     * Returns a list of customer objects based on limit and offset.
     * @param limitParam
     * @param offsetParam
     * @return
     */
    List<Customer> setOfCustomers(int limitParam, int offsetParam);

    /**
     * Checks database and returns the country with most customers.
     * @return
     */
    CustomerCountry countryWhitMostCustomers();

    /**
     * Checks database and returns the customer with most total spent
     * after calculating total invoices.
     * @return
     */
    CustomerSpender returnBiggestSpender();

    /**
     * Takes in User-id and checks for favourite genre.
     * Returns a customerGenre object that contains
     * a customer object, and a list of genre name and total orders.
     * @param input
     * @return
     */
    CustomerGenre returnCustomerGenre(int input);
}
