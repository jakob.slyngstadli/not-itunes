package no.accelerate.notitunes.repository;

import no.accelerate.notitunes.models.Customer;
import no.accelerate.notitunes.models.CustomerCountry;
import no.accelerate.notitunes.models.CustomerGenre;
import no.accelerate.notitunes.models.CustomerSpender;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Repository;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

@Repository
public class CustomerRepositoryImpl implements CustomerRepository {

    private final String url;
    private final String username;
    private final String password;

    public CustomerRepositoryImpl(
            @Value("${spring.datasource.url}") String url,
            @Value("${spring.datasource.username}") String username,
            @Value("${spring.datasource.password}") String password) {
        this.url = url;
        this.username = username;
        this.password = password;
    }

    @Override
    public List<Customer> findAll() {
        String sql = "SELECT * FROM customer";
        List<Customer> customerList = new ArrayList<>();
        try(Connection conn = DriverManager.getConnection(url, username,password)) {
            // Write statement
            PreparedStatement statement = conn.prepareStatement(sql);
            // Execute statement
            ResultSet result = statement.executeQuery();
            // Handle result
            while(result.next()) {
                Customer customer = new Customer(
                        result.getInt("customer_id"),
                        result.getString("first_name"),
                        result.getString("last_name"),
                        result.getString("country"),
                        result.getString("postal_code"),
                        result.getString("phone"),
                        result.getString("email")
                );
                customerList.add(customer);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return customerList;
    }

    @Override
    public Customer findById(Integer cus_id) {
        Customer customer = null;
        String sql = "SELECT * FROM customer WHERE customer_id = ?";
        try(Connection conn = DriverManager.getConnection(url, username,password)) {
            // Write statement
            PreparedStatement statement = conn.prepareStatement(sql);
            statement.setInt(1, cus_id);
            // Execute statement
            ResultSet result = statement.executeQuery();
            result.next();
                customer = new Customer(
                        result.getInt("customer_id"),
                        result.getString("first_name"),
                        result.getString("last_name"),
                        result.getString("country"),
                        result.getString("postal_code"),
                        result.getString("phone"),
                        result.getString("email")
                );
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return customer;
    }

    @Override
    public int insert(Customer newCustomer) {
        int result = 0;
        String sql = "INSERT INTO customer (first_name, last_name, country, postal_code, phone, email) " +
                "values(?,?,?,?,?,?)";
        try(Connection conn = DriverManager.getConnection(url, username,password)) {
            // Write statement
            PreparedStatement statement = conn.prepareStatement(sql);
            statement.setString(1, newCustomer.first_name());
            statement.setString(2, newCustomer.last_name());
            statement.setString(3, newCustomer.country());
            statement.setString(4, newCustomer.postal_code());
            statement.setString(5, newCustomer.phone());
            statement.setString(6, newCustomer.email());

            // Execute statement
            result = statement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return result;
    }

    @Override
    public int update(Customer newCustomer) {
        int result = 0;
        String sql = "UPDATE customer SET first_name = ?, last_name = ?, country = ?, postal_code = ?, phone = ?, email = ? WHERE customer_id = ?;";
        try(Connection conn = DriverManager.getConnection(url, username,password)) {
            // Write statement
            PreparedStatement statement = conn.prepareStatement(sql);
            statement.setString(1, newCustomer.first_name());
            statement.setString(2, newCustomer.last_name());
            statement.setString(3, newCustomer.country());
            statement.setString(4, newCustomer.postal_code());
            statement.setString(5, newCustomer.phone());
            statement.setString(6, newCustomer.email());
            statement.setInt(7, newCustomer.customer_id());

            // Execute statement
            result = statement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return result;


    }

    @Override
    public int delete(Customer object) {
        return 0;
    }

    @Override
    public int deleteById(Integer id) {
        return 0;
    }


    @Override
    public Customer findByName(String firstName, String lastName) {
        Customer customer = null;
        String sql = "SELECT * FROM customer WHERE first_name = ? AND last_name = ?";
        try(Connection conn = DriverManager.getConnection(url, username,password)) {
            // Write statement
            PreparedStatement statement = conn.prepareStatement(sql);
            statement.setString(1, firstName);
            statement.setString(2, lastName);
            // Execute statement
            ResultSet result = statement.executeQuery();
            result.next();
            customer = new Customer(
                    result.getInt("customer_id"),
                    result.getString("first_name"),
                    result.getString("last_name"),
                    result.getString("country"),
                    result.getString("postal_code"),
                    result.getString("phone"),
                    result.getString("email")
            );
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return customer;
    }

    @Override
    public List<Customer> setOfCustomers(int limitParam, int offsetParam) {
        String sql = "SELECT * FROM customer ORDER BY customer_id LIMIT ? OFFSET ?";
        List<Customer> customerList = new ArrayList<>();
        try(Connection conn = DriverManager.getConnection(url, username,password)) {
            // Write statement
            PreparedStatement statement = conn.prepareStatement(sql);

            statement.setInt(1, limitParam);
            statement.setInt(2, offsetParam);

            // Execute statement
            ResultSet result = statement.executeQuery();
            // Handle result
            while(result.next()) {
                Customer customer = new Customer(
                        result.getInt("customer_id"),
                        result.getString("first_name"),
                        result.getString("last_name"),
                        result.getString("country"),
                        result.getString("postal_code"),
                        result.getString("phone"),
                        result.getString("email")
                );
                customerList.add(customer);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return customerList;
    }

    @Override
    public CustomerCountry countryWhitMostCustomers() {
        String sql = "SELECT COUNT(country), Country FROM customer group by Country;";
        CustomerCountry customerCountry = new CustomerCountry(0,"");
        String tempCountry = "";
        try(Connection conn = DriverManager.getConnection(url, username,password)) {
            // Write statement
            PreparedStatement statement = conn.prepareStatement(sql);

            // Execute statement
            ResultSet result = statement.executeQuery();

            while(result.next()) {
                CustomerCountry customer = new CustomerCountry(
                        result.getInt("count"),
                        result.getString("Country")
                );
                if (customer.count()> customerCountry.count()) {
                    customerCountry = customer;
                }
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return customerCountry;
    }

    @Override
    public CustomerSpender returnBiggestSpender() {
        CustomerSpender customer = null;
        String sql = "SELECT customer_id,sum(total) as sumTotal FROM invoice group by customer_id order by sumTotal desc limit 1;";
        try(Connection conn = DriverManager.getConnection(url, username,password)) {
            // Write statement
            PreparedStatement statement = conn.prepareStatement(sql);

            // Execute statement
            ResultSet result = statement.executeQuery();
            result.next();
            Customer tempCustomer = null;
            customer = new CustomerSpender(
                    tempCustomer = findById(result.getInt("customer_id")),
                    result.getDouble("sumTotal")
            );
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return customer;
    }

    @Override
    public CustomerGenre returnCustomerGenre(int input) {
        String sql = "       SELECT COUNT(t.genre_id) as count, g.name" +
                "        FROM invoice as i" +
                "        inner join invoice_line as il ON i.invoice_id = il.invoice_id" +
                "        inner join track as t on il.track_id = t.track_id" +
                "        inner join genre as g on t.genre_id = g.genre_id" +
                "        where customer_id = ?" +
                "        group by g.name" +
                "        order by count desc NULLS LAST FETCH FIRST 1 ROWS WITH TIES;";
        List<String> genreList = new ArrayList<>();
        try(Connection conn = DriverManager.getConnection(url, username,password)) {
            // Write statement
            PreparedStatement statement = conn.prepareStatement(sql);

            statement.setInt(1, input);

            // Execute statement
            ResultSet result = statement.executeQuery();
            // Handle result
            while(result.next()) {
                String tempString = result.getString("name");
                genreList.add(tempString);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        CustomerGenre customer = new CustomerGenre(findById(input),genreList);
        return customer;

    }

}
