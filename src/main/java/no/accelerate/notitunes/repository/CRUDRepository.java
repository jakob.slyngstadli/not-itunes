package no.accelerate.notitunes.repository;

import java.util.List;

public interface CRUDRepository <T,U> {
    /**
     * Returns every customer.
     * @return
     */
    List<T> findAll();

    /**
     * Finds specific customer by Id.
     * @param id
     * @return
     */
    T findById(U id);

    /**
     * Insert customer object into database.
     * Takes id parameter, but doesn't implement it.
     * @param object
     * @return
     */
    int insert(T object);

    /**
     * Checks a customer by id,
     * changes values according to parameters set.
     * @param object
     * @return
     */
    int update(T object);
    int delete(T object);
    int deleteById(U id);
}
