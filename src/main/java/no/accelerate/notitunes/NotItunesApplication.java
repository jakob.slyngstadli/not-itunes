package no.accelerate.notitunes;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class NotItunesApplication {



    public static void main(String[] args) {
        SpringApplication.run(NotItunesApplication.class, args);


    }

}
