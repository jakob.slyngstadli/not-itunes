package no.accelerate.notitunes.runners;

import no.accelerate.notitunes.models.Customer;
import no.accelerate.notitunes.repository.CustomerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Component;

@Component
public class PgAppRunner implements ApplicationRunner {

    private final CustomerRepository customerRepository;

    @Autowired
    public PgAppRunner(CustomerRepository customerRepository) {
        this.customerRepository = customerRepository;
    }

    @Override
    public void run(ApplicationArguments args) throws Exception {
        //Add methods here
        Customer evenNymann = new Customer(0, "Even", "Nymann", "Norje", "3044", "40600205", "even@nymann.exe");
        System.out.println(customerRepository.insert(evenNymann));
    }
}
