package no.accelerate.notitunes.models;

import java.util.List;

public record CustomerGenre(Customer customer, List<String> genre) {
}
