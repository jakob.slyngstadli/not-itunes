package no.accelerate.notitunes.models;

public record CustomerCountry(int count, String country) {
}
