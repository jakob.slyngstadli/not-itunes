package no.accelerate.notitunes.models;

public record CustomerSpender(Customer customer, double total) {
}
