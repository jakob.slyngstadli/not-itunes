# Not Itunes
This project is a direct alternative to itunes.
Using this framework, it is possible to store vast amounts of information about the user,
their invoices and favourite genre. This in turn lets you devise devious plots and thus
capitalize on the huge market that is music streaming.

Not Itunes already has a small database as a proof of concept, 
ready to be implemented into your system.

## How to use Not Itunes
    > findAll(): returns list with customers
    > findById(): returns customer using ID.
    > insert(): inserts custom customer into database.
    > update(): updates an excisting customer.
    > findByName(): returns customer using first and last name.
    > setOfCustomers(): returns a list of customers based on limit and offset.
    > countryWithMostCustomer(): returns the country with most customers.
    > returnBiggestSpender(): checks spending habits to check biggest spender.
    > returnCustinerGenre(): checks spending habits to find favourite genre.

## To run this project
    >Install Java 17
    >Clone repo
    >Open in intellij
    >Run in main

### Author
Co-created by Truls Ombye Hafnor and Jakob Slyngstadli
